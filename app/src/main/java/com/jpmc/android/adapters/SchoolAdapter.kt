package com.jpmc.android.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jpmc.android.databinding.SchoolItemBinding
import com.jpmc.android.datas.SchoolsItem
import com.jpmc.android.utils.setTextOrHide

class SchoolAdapter(private val context: Context, private var schools: List<SchoolsItem>) :
    RecyclerView.Adapter<SchoolAdapter.SearchHolderView>() {
    var onItemClick: ((SchoolsItem) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchHolderView {
        val itemBinding = SchoolItemBinding.inflate(LayoutInflater.from(context), parent, false)
        return SearchHolderView(itemBinding)
    }

    override fun onBindViewHolder(holder: SearchHolderView, position: Int) {
        val itemResponse = schools[position]
        holder.itemView.setOnClickListener {
            onItemClick?.invoke(itemResponse)
        }
        holder.bind(itemResponse)
    }

    override fun getItemCount(): Int {
        return schools.size
    }

    class SearchHolderView(private val itemBinding: SchoolItemBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        fun bind(schoolsItem: SchoolsItem) {
            itemBinding.apply {
                schoolName.setTextOrHide(schoolsItem.school_name)
                schoolAddress.text = schoolsItem.toAddress()
            }
        }
    }
}