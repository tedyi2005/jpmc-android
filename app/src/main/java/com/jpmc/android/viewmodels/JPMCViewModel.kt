package com.jpmc.android.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jpmc.android.repositories.JPMCRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 *  MVVM ViewModel
 */
@ExperimentalCoroutinesApi
@HiltViewModel
class JPMCViewModel @Inject constructor(private val jPMCRepository: JPMCRepository) : ViewModel() {

    val schools = jPMCRepository.schools
    val satResult = jPMCRepository.satResult

    fun getSchools() {
        viewModelScope.launch {
            jPMCRepository.getSchools()
        }
    }

    fun getSATResult(dbn: String) {
        viewModelScope.launch {
            jPMCRepository.getSATResult(dbn)
        }
    }
}