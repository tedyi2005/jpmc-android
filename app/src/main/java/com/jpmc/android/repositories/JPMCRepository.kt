package com.jpmc.android.repositories

import com.jpmc.android.apis.JPMCApis
import com.jpmc.android.apis.UIResult
import com.jpmc.android.datas.SchoolResult
import com.jpmc.android.datas.SchoolResultItem
import com.jpmc.android.datas.Schools
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import java.lang.Exception
import javax.inject.Inject

/**
 *  MVVM Repository
 */
@ExperimentalCoroutinesApi
class JPMCRepository @Inject constructor(private val jPMCApis: JPMCApis) {

    private val _schools = MutableStateFlow<UIResult<Schools>>(UIResult.Loading(false))
    val schools: StateFlow<UIResult<Schools>> get() = _schools

    private val _satResult = MutableStateFlow<UIResult<SchoolResult>>(UIResult.Loading(false))
    val satResult: StateFlow<UIResult<SchoolResult>> get() = _satResult

    // repository method
    suspend fun getSchools() {
        try {
            _schools.value = UIResult.Loading(true)
            val response = jPMCApis.getSchools()
            _schools.value = UIResult.Loading(false)
            _schools.value = UIResult.Success(response)
        } catch (exception: Exception) {
            _schools.value = exception.message?.let { UIResult.Error(it) }!!
        }
    }

    suspend fun getSATResult(dbn: String) {
        try {
            _satResult.value = UIResult.Loading(true)
            val response = jPMCApis.getSATResult(dbn)
            _satResult.value = UIResult.Loading(false)
            _satResult.value = UIResult.Success(response)
        } catch (exception: Exception) {
            _satResult.value = exception.message?.let { UIResult.Error(it) }!!
        }
    }
}